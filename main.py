"""
XVSO app
"""
__version__ = '0.5'

import os

from kivy.uix.screenmanager import ScreenManager
from kivymd.app import MDApp

from src.screens import GameScreen, MenuScreen


class XVSOApp(MDApp):
    """
    Главный модуль приложения
    """
    def build_config(self, config):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        SOURCE_DIR = os.path.join(BASE_DIR, 'src')
        KV_DIR = os.path.join(SOURCE_DIR, 'kv')
        DIRS = {
            "base_dir": BASE_DIR,
            "source_dir": SOURCE_DIR,
            "kv_dir": KV_DIR,
        }

        config.setdefaults('dirs', DIRS)

    def get_kv_dir(self):
        return app.config.get('dirs', 'kv_dir')

    def build(self):
        screen_manager = ScreenManager()
        screen_manager.add_widget(MenuScreen(name='menu'))
        screen_manager.add_widget(GameScreen(name='game'))
        return screen_manager


if __name__ == '__main__':
    app = XVSOApp()
    app.run()
