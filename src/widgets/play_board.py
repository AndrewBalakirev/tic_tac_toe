from kivy.uix.gridlayout import GridLayout

from src.widgets.place import Place


class PlayBoard(GridLayout):
    cols = 3
    rows = 3

    def generate_field(self, callback: callable) -> None:
        for place_id in range(9):
            place = Place(place_id=place_id, text='', on_press=callback)
            self.add_widget(place)

    def get_place(self, place_id: int):
        for place in self.children:
            if place.place_id == place_id:
                return place

    def clear(self) -> None:
        for place in self.children:
            place.text = ''

    def set_place_text(self, place: Place, text: str) -> None:
        place.text = text

    def is_empty_place(self, place: Place) -> bool:
        return not self.is_filled_place(place)

    def is_filled_place(self, place: Place) -> bool:
        return bool(place.text)

    def is_all_places_filled(self) -> bool:
        return all(map(self.is_filled_place, self.children))

    def get_places_text(self, places_id_list: list) -> list:
        return [self.get_place(place_id).text for place_id in places_id_list]
