from kivy.properties import DictProperty
from kivymd.uix.label import MDLabel

from src.player import SinglePlayer


class ScoreLabel(MDLabel):
    size_hint = (1, .2)
    font_size = 20
    halign = 'center'
    valign = 'middle'
    score_values = DictProperty({})

    def add_point_to_winner(self, winner: SinglePlayer):
        self.score_values[winner.token] += 1

    def on_score_values(self, _, value):
        self.text = f'X  {value["X"]} : {value["O"]}  O'

    def init_score(self, player1: SinglePlayer, player2: SinglePlayer):
        self.score_values = {
            player1.token: 0,
            player2.token: 0
        }


class AttackerLabel(MDLabel):
    size_hint = (1, .1)
    halign = 'center'
    valign = 'middle'

    def set_attacker_text(self, attacker: SinglePlayer):
        self.text = f'Ходит {attacker.token}'
