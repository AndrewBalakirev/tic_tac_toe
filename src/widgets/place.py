from kivy.properties import NumericProperty
from kivy.uix.button import Button


class Place(Button):
    place_id = NumericProperty()
