from src.player import SinglePlayer
from src.screens.mixins import GameScreenMixin


class GameScreen(GameScreenMixin):
    player = SinglePlayer('X')
    opponent = SinglePlayer('O')
