import os

from kivy.lang.builder import Builder
from kivy.uix.screenmanager import Screen
from kivymd.app import MDApp
from kivymd.uix.button import MDFlatButton

from src.screens.end_game_dialog import EndGameDialog
from src.widgets.place import Place


class ScreenMixin(Screen):
    kv_file = None

    def __init__(self, **kwargs):
        self._set_kv_file_to_builder()
        super().__init__(**kwargs)

    @property
    def _kv_full_path(self):
        app = MDApp.get_running_app()
        kv_dir = app.get_kv_dir()
        return os.path.join(kv_dir, self.kv_file)

    def _set_kv_file_to_builder(self):
        kv_file = self._kv_full_path
        if kv_file not in Builder.files:
            Builder.load_file(kv_file)


class GameScreenMixin(ScreenMixin):
    kv_file = 'game.kv'

    _winner = None
    player = None
    opponent = None
    _field_grid = None
    _score_label = None
    _attacker_label = None
    _attacker = player

    @property
    def _players(self):
        return (self.player, self.opponent)

    def on_enter(self, **_kwargs):
        if self._field_grid is None:
            self._field_grid = self.ids['field_grid']
            self._field_grid.generate_field(self._place_click)

        self._score_label = self.ids['score_text']
        self._attacker_label = self.ids['attacker_text']
        self._drop_state()

    def _drop_state(self):
        self._attacker = self._get_x_player()
        self._attacker_label.set_attacker_text(self._attacker)
        self._score_label.init_score(self.player, self.opponent)

    def _get_x_player(self):
        for player in self._players:
            if player.token == 'X':
                return player

    def _switch_attacker(self):
        if self.player != self._attacker:
            self._attacker = self.player
        else:
            self._attacker = self.opponent

    def _is_right_move(self, place: Place):
        return self._field_grid.is_empty_place(place)

    def _place_click(self, place: Place):
        if not self._is_right_move(place):
            return

        token = self._attacker.token
        self._field_grid.set_place_text(place, token)
        self._check_win()
        self._switch_attacker()
        self._attacker_label.set_attacker_text(self._attacker)

    def _check_nobody_win(self):
        if self._field_grid.is_all_places_filled():
            self.dialog = EndGameDialog(
                title='Ничья',
                text='Продолжить?',
                buttons=[
                    MDFlatButton(
                        text="Нет",
                        on_press=self._not_continue_game
                    ),
                    MDFlatButton(
                        text="Да",
                        on_press=self._continue_game
                    ),
                ],
            )
            self.dialog.open()

    def _check_win(self):
        win_combinations = (
            (0, 1, 2),
            (3, 4, 5),
            (6, 7, 8),

            (0, 3, 6),
            (1, 4, 7),
            (2, 5, 8),

            (2, 4, 6),
            (0, 4, 8),
        )

        for comb in win_combinations:
            places_texts = self._field_grid.get_places_text(comb)
            if places_texts.count(self._attacker.token) == 3:
                self._winner = self._attacker
                break

        if self._winner:
            self.dialog = EndGameDialog(
                title=f'Выйграл "{self._winner.token}"',
                text='Продолжить?',
                buttons=[
                    MDFlatButton(
                        text="Нет",
                        on_press=self._not_continue_game
                    ),
                    MDFlatButton(
                        text="Да",
                        on_press=self._continue_game
                    ),
                ],
            )
            self.dialog.open()
            self._score_label.add_point_to_winner(self._winner)
            self._winner = None
        else:
            self._check_nobody_win()

    def _not_continue_game(self, *_args):
        self._stop()

    def _continue_game(self, *_args):
        self._field_grid.clear()
        self.dialog.close()
        self.dialog = None

    def _stop(self):
        if self.dialog:
            self.dialog.close()
            self.dialog = None

        self._field_grid.clear()
        self._drop_state()
        self.manager.current = 'menu'
